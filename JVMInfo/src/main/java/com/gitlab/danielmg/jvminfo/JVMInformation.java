package com.gitlab.danielmg.jvminfo;

import java.lang.instrument.Instrumentation;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;

/**
 * Methods for interrogating the JVM
 * 
 * Done as Enum singleton pattern to ensure thread safety
 * 
 * @author Daniel Matthews-Grout
 *
 */
public enum JVMInformation {

	INSTANCE;

	private static final int MAX_THREAD_DEPTH = 100; // number of stack frames
	private static final String HOTSPOT_BEAN_NAME = "com.sun.management:type=HotSpotDiagnostic";

	/**
	 * Helper method - you can just use the Enum too
	 * 
	 * @return an instance of JVMInformations
	 */
	public static JVMInformation getInstance() {
		return INSTANCE;
	}

	private final ThreadMXBean threadMX = ManagementFactory.getThreadMXBean();
	private final MemoryMXBean memoryMX = ManagementFactory.getMemoryMXBean();
	private final List<MemoryPoolMXBean> memoryPoolMXs = ManagementFactory.getMemoryPoolMXBeans();
	private final List<String> memoryNotifications = Collections.synchronizedList(new LinkedList<String>());
	private final RuntimeMXBean runtimeMX = ManagementFactory.getRuntimeMXBean();
	private static Instrumentation instrumentation;
	private final String vendor = System.getProperty("java.vm.vendor");

	/**
	 * <p>
	 * Entry point for instrumentation - need to add this to MANIFEST.MF:
	 * </p>
	 * 
	 * <pre>
	 * Premain-Class: com.gitlab.danielmg.jvminfo.JVMInformation
	 * </pre>
	 * <p>
	 * Java command line needs this option -javaagent:whateverthisisin.jar
	 * </p>
	 * 
	 * @param args
	 * @param inst
	 */
	public static void premain(String args, Instrumentation i) {
		instrumentation = i;
	}

	/**
	 * @return all loaded classes
	 * @throws InstanceNotFoundException
	 *             if instrumentation not configured
	 */
	public Class<?>[] getAllLoadedClasses() throws InstanceNotFoundException {
		if (instrumentation == null)
			throw new InstanceNotFoundException("instrumentation not configured");
		return instrumentation.getAllLoadedClasses();
	}

	/**
	 * Note - this only does the object itself any member variables need to be sized
	 * 
	 * @param o
	 *            the object to size
	 * @return the size in bytes
	 * @throws InstanceNotFoundException
	 */
	public long getObjectSize(Object o) throws InstanceNotFoundException {
		if (instrumentation == null)
			throw new InstanceNotFoundException("instrumentation not configured");

		return instrumentation.getObjectSize(o);
	}

	/**
	 * @return classes that have been instantiated
	 * @throws InstanceNotFoundException
	 *             if instrumentation not configured
	 */
	public Class<?>[] getInstantiatedClasses() throws InstanceNotFoundException {
		if (instrumentation == null)
			throw new InstanceNotFoundException("instrumentation not configured");

		// Assuming that you want the classloader working here not the JVM System as
		// that might be too high
		return instrumentation.getInitiatedClasses(Thread.currentThread().getContextClassLoader());
	}

	/**
	 * @return a map of memory information strings keyed by pool name
	 */
	public synchronized Map<String, String> getMemoryPoolInformation() {
		Map<String, String> ret = new HashMap<String, String>();

		for (MemoryPoolMXBean m : memoryPoolMXs) {
			StringBuilder b = new StringBuilder();

			b.append("Name: ");
			b.append(m.getName());
			b.append('\n');

			b.append("Memory Type: ");
			b.append(m.getType().toString());
			b.append('\n');

			b.append("Managers: ");
			b.append(String.join(",", m.getMemoryManagerNames()));
			b.append('\n');

			if (m.getCollectionUsage() != null) {
				b.append("Memory Usage: ");
				b.append(m.getCollectionUsage().toString());
				b.append('\n');
			}

			if (m.getPeakUsage() != null) {
				b.append("Peak Usage: ");
				b.append(m.getPeakUsage().toString());
				b.append('\n');
			}

			ret.put(m.getName(), b.toString());
		}

		return ret;
	}

	/**
	 * Configures the listener and clears the current list also sets verbose output
	 * - such as GC notifications
	 */
	public synchronized void initialiseMemoryThresholdNotifications(long usageThresholdBytes,
			long collectionThresholdBytes) {
		memoryNotifications.clear();
		memoryMX.setVerbose(true);
		((NotificationEmitter) memoryMX).addNotificationListener(new MemoryNotificationListener(), null, null);

		for (MemoryPoolMXBean m : memoryPoolMXs) {
			if (!m.getType().equals(MemoryType.HEAP)) {
				if (!vendor.equalsIgnoreCase("IBM Corporation")) {
					m.setUsageThreshold(usageThresholdBytes);
				}
			} else {
				m.setCollectionUsageThreshold(collectionThresholdBytes);
			}

		}
	}

	/**
	 * @return a list of received memory notifications
	 */
	public List<String> getMemoryNotifications() {
		return memoryNotifications;
	}

	/**
	 * @return a Map of thread details the key is thread name
	 */
	public synchronized Map<String, String> getThreadDump() {
		Map<String, String> ret = new HashMap<String, String>();

		for (ThreadInfo t : threadMX.getThreadInfo(threadMX.getAllThreadIds(), MAX_THREAD_DEPTH))
			ret.put(t.getThreadName(), t.toString());

		return ret;
	}

	public String getPlaformInformation() {
		StringBuilder b = new StringBuilder();

		b.append("Boot Class Path: ");
		b.append(runtimeMX.getBootClassPath());
		b.append('\n');

		b.append("Class Path: ");
		b.append(runtimeMX.getClassPath());
		b.append('\n');

		b.append("Start time: ");
		b.append(new Date(runtimeMX.getStartTime()));
		b.append('\n');

		b.append("Uptime: ");
		b.append(runtimeMX.getUptime() / 1000);
		b.append('\n');

		b.append("Vendor: ");
		b.append(System.getProperty("java.vm.vendor"));
		b.append('\n');

		return b.toString();
	}

	/**
	 * Initiates dumps of varying sort -JVM Vendor specific
	 */
	public void takeADump() {

		if (vendor.equalsIgnoreCase("Oracle Corporation")) {
			doOracleDump();
		}

		if (vendor.equalsIgnoreCase("IBM Corporation")) {
			doIBMDump();
		}

	}

	/*
	 * As the dumpings are vendor specific we don't want to have references to
	 * classes that will not exist so we need to do a lot of evil looking stuff here
	 * with reflection to force out a good old dump
	 */
	private static final DateFormat sdf = new SimpleDateFormat("ddmmyyy_hhmmss");
	private void doOracleDump() {
		try {
			String fileName="heap_dump_" + sdf.format(Calendar.getInstance().getTime()) + ".dat";
			Class<?> c = Class.forName("com.sun.management.HotSpotDiagnosticMXBean");
			MBeanServer server = ManagementFactory.getPlatformMBeanServer();
			Method m = c.getMethod("dumpHeap", String.class, boolean.class);
			m.invoke(ManagementFactory.newPlatformMXBeanProxy(server, HOTSPOT_BEAN_NAME, c), fileName, true);
			System.out.println("hotspot dump: " + fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void doIBMDump() {
		try {
			Class<?> c = Class.forName("com.ibm.jvm.Dump");
			c.getMethod("HeapDump", (Class<?>[]) null).invoke(null);
			c.getMethod("JavaDump", (Class<?>[]) null).invoke(null);
			c.getMethod("SnapDump", (Class<?>[]) null).invoke(null);
			c.getMethod("SystemDump", (Class<?>[]) null).invoke(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class MemoryNotificationListener implements NotificationListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * javax.management.NotificationListener#handleNotification(javax.management.
		 * Notification, java.lang.Object)
		 */
		public void handleNotification(Notification notification, Object handback) {
			memoryNotifications.add(Calendar.getInstance().getTime() + notification.toString());
		}

	}

}
