package com.gitlab.danielmg.jvminfo;

import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

public class JVMInformationTest extends TestCase {

	public void testGetMemoryPoolInformation() {

		for (String s : JVMInformation.INSTANCE.getMemoryPoolInformation().values())
			System.out.println(s);

	}

	public void testTakeADump() {
		JVMInformation.INSTANCE.takeADump();
	}
	
	public void testGetMemoryNotifications() throws InterruptedException {

		JVMInformation.INSTANCE.initialiseMemoryThresholdNotifications(1000, 1000);
		List<String> l = new LinkedList<String>();
		// spawn a load of stuff to generate memory use
		for (int i = 0; i < 5000000; i++) {
			String s = "i" + i;
			l.add(s);
		}
		l.clear();

		for (String n : JVMInformation.INSTANCE.getMemoryNotifications())
			System.out.println(n);

	}

	public void testGetThreadDump() {
		for (String s : JVMInformation.INSTANCE.getThreadDump().values())
			System.out.println(s);
	}

	public void testGetPlatformInformation() {
		System.out.println(JVMInformation.INSTANCE.getPlaformInformation());
	}
	

}
